FROM python:3.6.4

RUN apt-get update -qq && apt-get install --no-install-recommends -y \
        curl \
        unzip \
        git \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app/src

COPY requirements.txt /app/requirements.txt

RUN pip install --upgrade pip && pip install -r /app/requirements.txt

RUN curl "http://nlp.ailab.lv:9090/ner/lv_ner_sembank0.4-c0.1-m3-run0.zip" --output model.zip \
    && mkdir -p /model \
    && unzip model.zip -d /model \
    && rm model.zip

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV LANGUAGE=C.UTF-8
ENV PYTHONIOENCODING=utf-8
ENV PYTHONUNBUFFERED=1
ENV PORT=80
ENV MODEL=/model

COPY src /app/src/

CMD if [ ${PY_ENV} = "dev" ]; then \
    python -m tag.server; \
else \
    gunicorn tag.server:app -c gunicorn_config.py -w ${GUNICORN_WORKERS:=1} -t ${GUNICORN_TIMEOUT:=60} --graceful-timeout ${GUNICORN_TIMEOUT:=60} -b ${HOST:=0.0.0.0}:${PORT:=80} ${GUNICORN_RELOAD:=""} --log-level ${GUNICORN_LOG_LEVEL:=info}; \
fi
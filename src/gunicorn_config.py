import os

LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')
LOG_FORMAT = os.getenv('LOG_FORMAT', 'default')

logconfig_dict = {
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] [%(process)d] [%(levelname)s] [%(name)s] [%(pathname)s:%(lineno)d] %(message)s',
        },
        'default': {
            'format': '[%(asctime)s] [%(process)d] [%(levelname)s] [%(name)s] %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': LOG_FORMAT,
        }
    },
    'loggers': {
        # 'server': {'level': 'INFO'},
        'gunicorn': {'handlers': ['console'], 'level': 'INFO', 'propagate': False},
    },
    'root': {
        'level': LOG_LEVEL,
        'handlers': ['console']
    },
    'version': 1,
    'disable_existing_loggers': False,
}

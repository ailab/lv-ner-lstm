from abc import abstractmethod
from collections import defaultdict
import json
import re
from typing import TypeVar, Type, Dict, List
import logging
import math
import numpy as np
from keras import backend as K
from keras import layers, initializers

from tag.embeddings import Embeddings, get_embedding_weights
from tag.vocab import Vocab

logger = logging.getLogger('tag.embedder')


class Embedder(object):
    def __init__(self, config):
        if config:
            input_config = config['inputs']
            output_config = config['output']
            self.inputs = []
            for f in input_config:
                self.inputs.append(get_feature(f['type']).load(f))
            self.output = get_feature(output_config['type']).load(output_config)

    def fit(self, sentences):
        for f in self.inputs:
            f.fit(sentences)
        self.output.fit(sentences)

    def transform(self, sentences, with_outputs=False):
        inputs = []
        outputs = []

        for sentence in sentences:
            sentence_inputs = []
            for f in self.inputs:
                sentence_inputs.append(np.asarray(f.transform(sentence), dtype='float32'))

            if with_outputs:
                sentence_outputs = np.asanyarray(self.output.transform(sentence), dtype='float32')
            else:
                sentence_outputs = None

            outputs.append(sentence_outputs)
            inputs.append(sentence_inputs)

        return inputs, outputs

    def get_labels(self, sentence_outputs):
        id_label = {i: t for t, i in self.output.voc.items()}
        return [id_label.get(label_id) for label_id in sentence_outputs]

    def save(self, path):
        r = {}
        r['inputs'] = [f.save() for f in self.inputs]
        r['output'] = self.output.save()
        with open(path, 'w') as f:
            f.write(json.dumps(r, indent=2, ensure_ascii=False))

    @classmethod
    def load(cls, path):
        p = cls(None)
        with open(path) as f:
            serialized = json.load(f)
            for k, v in serialized.items():
                if k == 'inputs':
                    v = [get_feature(f['type']).load(f) for f in v]
                elif k == 'output':
                    v = get_feature(v['type']).load(v)
                setattr(p, k, v)
        return p


T = TypeVar('T')


class Feature(object):
    _registry: Dict[Type, Dict[str, Type]] = defaultdict(dict)
    default_implementation: str = None

    @classmethod
    def register(cls: Type[T], name: str):
        registry = Feature._registry[cls]

        def add_subclass_to_registry(subclass: Type[T]):
            # Add to registry, raise an error if key has already been used.
            if name in registry:
                message = "Cannot register %s as %s; name already in use for %s" % (name, cls.__name__, registry[name].__name__)
                raise Exception(message)
            registry[name] = subclass
            return subclass

        return add_subclass_to_registry

    @classmethod
    def by_name(cls: Type[T], name: str) -> Type[T]:
        logger.info(f"instantiating registered subclass {name} of {cls}")
        if name not in Feature._registry[cls]:
            raise Exception("%s is not a registered name for %s" % (name, cls.__name__))
        return Feature._registry[cls].get(name)

    @classmethod
    def list_available(cls) -> List[str]:
        """List default first if it exists"""
        keys = list(Feature._registry[cls].keys())
        default = cls.default_implementation

        if default is None:
            return keys
        elif default not in keys:
            message = "Default implementation %s is not registered" % default
            raise Exception(message)
        else:
            return [default] + [k for k in keys if k != default]

    @abstractmethod
    def fit(self, sent): pass

    @abstractmethod
    def transform(self, sent): pass

    def layer(self): pass

    def save(self):
        return {k: v for k, v in self.__dict__.items() if not k.startswith('_')}

    @classmethod
    def load(cls, attributes=None):
        p = cls()
        if attributes:
            for k, v in attributes.items():
                setattr(p, k, v)
        return p


def get_feature(name):
    return Feature.by_name(name)


def list_features():
    return Feature.list_available()


UNK = '<UNK>'
PAD = '<PAD>'


@Feature.register("label")
class Label(Feature):
    def __init__(self):
        self.voc = None

    def fit(self, sentences):
        voc = Vocab()
        for s in sentences:
            voc.add(s['labels'])
        self.voc = voc.token2id
        logger.info('labels {} {}'.format(len(voc), voc.most_common()))
        return self

    def transform(self, sentence):
        labels = np.zeros((len(sentence['labels']), len(self.voc)), dtype=np.int32)
        for i, l in enumerate(sentence['labels']):
            labels[i, self.voc[l]] = 1
        return labels


def normalize_digits(token):
    return re.sub(r'[0-9０１２３４５６７８９]', r'0', token)


@Feature.register("embedding")
class TokenEmbedding(Feature):
    def __init__(self):
        self.voc = None
        self.filter = True
        self.file = None
        self._token_emb = None
        self.d = 100
        self.norm_lower = True
        self.norm_digits = True

    def get_embeddings(self):
        if not self._token_emb and self.file:
            self._token_emb = Embeddings.load(self.file, dim=self.d).token_emb
        return self._token_emb

    def fit(self, sentences):
        embeddings = self.get_embeddings() or {}
        voc = {PAD: 0, UNK: 1, '<nl>': 2}
        self.voc = voc
        if not self.filter:
            for w in embeddings:
                voc[w] = len(voc)
        for s in sentences:
            for t in s['tokens']:
                if self.norm_lower: t = t.lower()
                if self.norm_digits: t = normalize_digits(t)
                if t not in voc:
                    if self.filter and t in embeddings:
                        voc[t] = len(voc)
        logger.info('tokens[10] {} {}'.format(len(voc), sorted(list(voc.items()), key=lambda x: x[1])[:10]))
        return self

    def transform(self, sentence):
        tokens = []
        for token in sentence['tokens']:
            if token in self.voc:
                tokens.append(self.voc[token])
            elif token.lower() in self.voc:
                tokens.append(self.voc[token.lower()])
            else:
                tokens.append(self.voc[UNK])
        return tokens

    def layer(self, initialize_weights=False):
        ids = layers.Input(batch_shape=(None, None), dtype='int32')
        embeddings = None
        if initialize_weights:
            embeddings = self.get_embeddings()
        if initialize_weights and embeddings is not None:
            weights = get_embedding_weights(embeddings, self.voc, norm=False)
            weights[1] = np.random.uniform(-0.25, 0.25, weights.shape[1])
            if '<nl>' not in embeddings: weights[self.voc['<nl>']] = np.random.uniform(-0.25, 0.25, weights.shape[1])
            emb = layers.Embedding(weights.shape[0], weights.shape[1], trainable=False, mask_zero=True, weights=[weights])(ids)
        else:
            emb = layers.Embedding(len(self.voc), self.d, trainable=False, mask_zero=True)(ids)
        return ids, emb


def get_token_shape(token: str):
    r = []
    for c in token:
        if c.isdigit(): r.append('0')
        elif c.isalpha(): r.append('a' if c.islower() else 'A')
        elif c == '-': r.append('-')
        elif c == '.': r.append('.')
        else: r.append('x')
    if len(r) > 3:
        _r = [r[0]]
        for i, c in enumerate(r[1:]):
            if c != r[i]: _r.append(c)
        r = _r

    return ''.join(r)


@Feature.register("shape")
class Shape(Feature):
    def __init__(self):
        self.voc = None
        self.d = 10

    def fit(self, sentences):
        voc = Vocab().add([PAD, UNK])
        for s in sentences:
            voc.add(get_token_shape(t) for t in s['tokens'])
        self.voc = voc.token2id
        logger.info('shape_voc {} {}'.format(len(self.voc), sorted(list(self.voc.items()), key=lambda x: x[1])))

    def transform(self, sentence):
        return [self.voc.get(get_token_shape(t), self.voc[UNK]) for t in sentence['tokens']]

    def layer(self, **kwargs):
        scale = math.sqrt(3 / self.d)
        ids = layers.Input(batch_shape=(None, None), dtype='int32')
        emb = layers.Embedding(len(self.voc), self.d, mask_zero=True, embeddings_initializer=initializers.RandomUniform(-scale, scale))(ids)
        return ids, emb


def get_token_shape2(token: str):
    r = []
    for c in token:
        if c.isdigit(): r.append('0')
        elif c.isalpha(): r.append('a' if c.islower() else 'A')
        elif c == '-': r.append('-')
        elif c == '.': r.append('.')
        else: r.append('x')
    return ''.join(r)


@Feature.register("shape2")
class Shape2(Feature):
    def __init__(self):
        self.voc = None
        self.d = 30
        self.min_count = 5

    def fit(self, sentences):
        voc = Vocab().add([PAD, UNK]*100)
        for s in sentences:
            voc.add(get_token_shape2(t) for t in s['tokens'])
        voc.filter(min_count=self.min_count)
        self.voc = voc.token2id
        logger.info('shape2_voc {} {}'.format(len(self.voc), sorted(list(self.voc.items()), key=lambda x: x[1])))

    def transform(self, sentence):
        return [self.voc.get(get_token_shape(t), self.voc[UNK]) for t in sentence['tokens']]

    def layer(self, **kwargs):
        scale = math.sqrt(3 / self.d)
        ids = layers.Input(batch_shape=(None, None), dtype='int32')
        emb = layers.Embedding(len(self.voc), self.d, mask_zero=True, embeddings_initializer=initializers.RandomUniform(-scale, scale))(ids)
        return ids, emb


def get_token_shape3(token: str):
    r = []
    for c in token:
        if c.isdigit(): r.append('0')
        elif c.isalpha(): r.append('a' if c.islower() else 'A')
        elif c == '-': r.append('-')
        elif c == '.': r.append('.')
        elif c == ';': r.append(';')
        elif c == ',': r.append(',')
        elif c == ':': r.append(':')
        elif c == '"': r.append('"')
        else: r.append('x')
    _r = []
    counter = 0
    max_count = 4
    cp = None
    for c in r:
        if c == cp:
            counter += 1
        else:
            counter = 0
        if counter >= max_count and c != '0':
            continue
        else:
            _r.append(c)
    return ''.join(_r)


@Feature.register("shape3")
class Shape3(Feature):
    def __init__(self):
        self.voc = None
        self.min_count = 5

    def fit(self, sentences):
        voc = Vocab().add([PAD, UNK]*100)
        for s in sentences:
            voc.add(get_token_shape3(t) for t in s['tokens'])
        voc.filter(min_count=self.min_count)
        self.voc = voc.token2id
        logger.info('shape3_voc {} {}'.format(len(self.voc), sorted(list(self.voc.items()), key=lambda x: x[1])))

    def transform(self, sentence):
        r = np.zeros((len(sentence['tokens']), len(self.voc)))
        for i, t in enumerate(sentence['tokens']):
            r[i][self.voc.get(get_token_shape3(t), self.voc[UNK])] = 1
        return r

    def layer(self, **kwargs):
        ids = layers.Input(batch_shape=(None, None, len(self.voc)), dtype='float32')
        return ids, ids


def get_token_case(word: str):
    casing = 'other'

    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))

    if word.isdigit():  # Is a digit
        casing = 'numeric'
    elif digitFraction > 0.5:
        casing = 'mainly_numeric'
    elif word.islower():  # All lower case
        casing = 'allLower'
    elif word.isupper():  # All upper case
        casing = 'allUpper'
    elif word[0].isupper():  # is a title, initial char upper, then all lower
        casing = 'initialUpper'
    elif numDigits > 0:
        casing = 'contains_digit'

    return casing


@Feature.register("case")
class Case(Feature):
    def __init__(self):
        self.voc = None

    def fit(self, sentences):
        voc = Vocab().add([PAD, UNK])
        for s in sentences:
            voc.add(get_token_case(t) for t in s['tokens'])
        self.voc = voc.token2id
        logger.info('case_voc {} {}'.format(len(self.voc), sorted(list(self.voc.items()), key=lambda x: x[1])))

    def transform(self, sentence):
        r = np.zeros((len(sentence['tokens']), len(self.voc)))
        for i, t in enumerate(sentence['tokens']):
            r[i][self.voc.get(get_token_case(t), self.voc[UNK])] = 1
        return r

    def layer(self, **kwargs):
        ids = layers.Input(batch_shape=(None, None, len(self.voc)), dtype='float32')
        return ids, ids


@Feature.register("chars")
class Chars(Feature):
    def __init__(self):
        self.voc = None
        self.d = 25
        self.lstm_units = 25
        self.max_char_len = None
        self.min_count = 3

    def fit(self, sentences):
        max_char_len = self.max_char_len
        if max_char_len is None:
            max_char_len = 0
            for s in sentences:
                for t in s['tokens']:
                    max_char_len = max(max_char_len, len(t))
            self.max_char_len = max_char_len
            logger.info('max_char_len={}'.format(max_char_len))

        voc = Vocab().add([PAD, UNK]*100)
        for s in sentences:
            for t in s['tokens']:
                voc.add(t)
        voc.filter(min_count=3)
        self.voc = voc.token2id
        logger.info('chars {} {}'.format(len(self.voc), voc.most_common()))

    def transform(self, sentence):
        chars = []
        for token in sentence['tokens']:
            token_chars = [self.voc.get(c, 1) for c in token]
            token_chars = [0] * max(0, self.max_char_len - len(token)) + token_chars[:self.max_char_len]
            chars.append(token_chars)
        return chars

    def layer(self, **kwargs):
        ids = layers.Input(batch_shape=(None, None, None), dtype='int32')
        scale = math.sqrt(3 / self.d)
        char_emb = layers.Embedding(len(self.voc), self.d, mask_zero=True, embeddings_initializer=initializers.RandomUniform(-scale, scale))(ids)
        s = K.shape(char_emb)
        char_emb = layers.Lambda(lambda x: K.reshape(x, shape=(-1, s[-2], self.d)))(char_emb)
        char_emb = layers.Bidirectional(layers.LSTM(self.lstm_units))(char_emb)
        char_emb = layers.Lambda(lambda x: K.reshape(x, shape=[-1, s[1], 2 * self.lstm_units]))(char_emb)  # (batch size, max sentence length, char hidden size)
        return ids, char_emb


@Feature.register("whitespace")
class Whitespace(Feature):
    def __init__(self):
        self.voc = None
        self.c = 0

    def fit(self, sentences):
        voc = Vocab().add([PAD, UNK, 'NL', 'WS', 'NOWS'])
        self.voc = voc.token2id

    def transform(self, sentence):
        r = np.zeros((len(sentence['tokens']), len(self.voc)))
        prev_end = 0
        for i, (t, start, end) in enumerate(sentence['tokens_raw']):
            if '\n' in sentence['text'][prev_end:start]: r[i][self.voc.get('NL', 1)] = 1
            if prev_end == start : r[i][self.voc.get('NOWS', 1)] = 1
            if ' ' in sentence['text'][prev_end:start] or '\t' in sentence['text'][prev_end:start]: r[i][self.voc.get('WS', 1)] = 1
            prev_end = end
        return r

    def layer(self, **kwargs):
        ids = layers.Input(batch_shape=(None, None, len(self.voc)), dtype='float32')
        return ids, ids

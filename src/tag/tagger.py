import sys
import os
import json
import logging
import numpy as np
import shutil
import glob
import tarfile

from keras import layers, models, optimizers, losses, initializers
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger  #, TensorBoard

# from keras_contrib.layers import CRF
from keras_utils.layers import CRF

from tag.callbacks import F1score, TensorBoard
from tag.iterators import batch_iter
from tag import utils, scripts
from tag import config_utils as conf
from tag.embedder import Embedder

logger = logging.getLogger('tagger')


class Config(conf.BaseConfig):
    version = '0.2'
    description: conf.Str() = ''

    embedder: conf.Json() = None
    lstm_units: conf.IntList() = (100, 75)
    dropout: conf.Float() = 0.25
    recurrent_dropout: conf.Float() = 0.25
    loss: conf.Str(choices='crf categorical_crossentropy'.split()) = 'crf'

    # training
    dir: conf.Str() = None
    train: conf.Str() = None
    dev: conf.Str() = None
    batch_size: conf.Int() = 16
    epochs: conf.Int() = 50
    patience: conf.Int() = 10


class LstmTagger:
    def __init__(self, config: Config=None, embedder: Embedder=None):
        self.model = None   # type: models.Sequential
        self.embedder = embedder    # type: Embedder
        self.config = config or Config()
        self.train_config = dict()

    def build(self, verbose=False, initialize_weights=False):
        logger.info('Build model ..')
        c = self.config

        inputs = []
        token_inputs = []

        for f in self.embedder.inputs:
            ids, emb = f.layer(initialize_weights=initialize_weights)
            inputs.append(ids)
            token_inputs.append(emb)

        if len(token_inputs) > 1:
            x = layers.Concatenate()(token_inputs)
        else:
            x = token_inputs[0]

        for unit_count in c.lstm_units:
            x = layers.Bidirectional(layers.LSTM(unit_count, return_sequences=True, dropout=c.dropout, recurrent_dropout=c.recurrent_dropout))(x)

        if c.loss == 'crf':
            crf = CRF(len(self.embedder.output.voc))
            pred = crf(x)
            loss = crf.loss_function
        else:
            pred = layers.Dense(len(self.embedder.output.voc), activation='softmax')(x)
            loss = c.loss
        optimizer = optimizers.Nadam(clipnorm=5.)
        self.model = model = models.Model(inputs=inputs, outputs=[pred])
        logger.info('.. compile')
        model.compile(loss=loss, optimizer=optimizer)
        logger.info('.. finished compiling')

        if verbose:
            model.summary()
            # logger.info(model.get_config())
            logger.info("Optimizer: %s, %s" % (str(type(optimizer)), str(optimizer.get_config())))

    def save(self, path, name='model.h5', overwrite=True, config_only=False, include_src=False, model_only=False):
        logger.info('Save model to {} ..'.format(path))
        os.makedirs(path, exist_ok=True)
        if not model_only:
            self.config.save(os.path.join(path, 'config.json'))
            self.embedder.save(os.path.join(path, 'embedder.json'))
            if include_src:
                os.makedirs(os.path.join(path, 'src'), exist_ok=True)
                for f in glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)), '*.py')):
                    shutil.copy(f, os.path.join(path, 'src', os.path.basename(f)))
        if not config_only:
            # self.model.save(os.path.join(path, name), overwrite)
            self.model.save_weights(os.path.join(path, name), overwrite)
        logger.info('.. saved')

    @classmethod
    def load(cls, path, weights=True):
        logger.info('Loading model {} ..'.format(path))
        if path.endswith('.tar') or path.endswith('.tar.gz'):
            tmp_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'tmp', 'tmp_model'))
            logger.info('Extract to tmp directory: {}'.format(tmp_path))
            if os.path.exists(tmp_path): shutil.rmtree(tmp_path)
            os.makedirs(tmp_path, exist_ok=True)
            with tarfile.open(path, 'r:gz' if path.endswith('.gz') else 'r') as tar:
                tar.extractall(tmp_path)
            path = tmp_path

        if os.path.isdir(path):
            if os.path.exists(os.path.join(path, 'weights.h5')):
                path = os.path.join(path, 'weights.py')
            else:
                best_score = 0
                best_path = None
                for f in os.listdir(path):
                    if not 'weights' in f or not f.endswith('.h5'): continue
                    name = f[:-3]
                    score = float(name.rsplit('_')[-1])
                    if score > best_score:
                        best_score = score
                        best_path = os.path.join(path, f)
                if best_path:
                    logger.info('Use best model found ({}): {}'.format(best_score, best_path))
                    path = best_path

        config = Config.load(os.path.join(os.path.dirname(path), 'config.json'))
        embedder = Embedder.load(os.path.join(os.path.dirname(path), 'embedder.json'))
        m = cls(config=config, embedder=embedder)

        if not weights:
            m.model = models.load_model(path)
        else:
            m.build()
            m.model.load_weights(path)
        logger.info('.. loaded')
        return m

    def predict(self, sentences, out_conll=None, print_conll=False):
        res = []
        for s in sentences:
            x, _ = self.embedder.transform([s], with_outputs=False)
            x = x[0]
            predictions = self.model.predict([np.array([f]) for f in x])
            predictions = predictions.argmax(axis=-1)
            predictions = self.embedder.get_labels(predictions[0])
            res.append(predictions)

            s['ner'] = predictions

            if print_conll:
                for token, ner in zip(s['tokens'], s['ner']):
                    print('{}\t{}'.format(token, ner))
                print('')

        if out_conll:
            with open(out_conll, 'w') as f:
                for s in sentences:
                    for token, ner in zip(s['tokens'], s['ner']):
                        f.write('{}\t{}\n'.format(token, ner))
                    f.write('\n')

        return res

    def predict_spans(self, sentences):
        predictions = self.predict(sentences)

        spans = []
        for sent_idx, sent in enumerate(predictions):
            prev = None
            prev_start = -1
            for tok_idx, label in enumerate(sent):
                if label.startswith('B-'):
                    if prev:
                        span = dict(
                            label=prev,
                            start_idx=prev_start,
                            end_idx=tok_idx,
                            text=' '.join(sentences[sent_idx]['tokens'][prev_start:tok_idx]),
                            sent_idx=sent_idx
                        )
                        if 'start' in sentences[sent_idx]:
                            span['start'] = sentences[sent_idx]['start'][prev_start]
                        if 'end' in sentences[sent_idx]:
                            span['end'] = sentences[sent_idx]['end'][tok_idx-1]
                        spans.append(span)
                    prev = label[2:]
                    prev_start = tok_idx
                elif label.startswith('I-'):
                    if label[2:] != prev:
                        if prev:
                            span = dict(
                                label=prev,
                                start_idx=prev_start,
                                end_idx=tok_idx,
                                text=' '.join(sentences[sent_idx]['tokens'][prev_start:tok_idx]),
                                sent_idx=sent_idx
                            )
                            if 'start' in sentences[sent_idx]:
                                span['start'] = sentences[sent_idx]['start'][prev_start]
                            if 'end' in sentences[sent_idx]:
                                span['end'] = sentences[sent_idx]['end'][tok_idx - 1]
                            spans.append(span)
                        prev = label[2:]
                        prev_start = tok_idx
                elif prev:
                    span = dict(
                        label=prev,
                        start_idx=prev_start,
                        end_idx=tok_idx,
                        text=' '.join(sentences[sent_idx]['tokens'][prev_start:tok_idx]),
                        sent_idx=sent_idx
                    )
                    if 'start' in sentences[sent_idx]:
                        span['start'] = sentences[sent_idx]['start'][prev_start]
                    if 'end' in sentences[sent_idx]:
                        span['end'] = sentences[sent_idx]['end'][tok_idx - 1]
                    spans.append(span)
                    prev = None
        return spans

    def evaluate(self, sentences, output=True, compact=False, comment='', header=True, file=sys.stderr, output_comp=None):
        labels = [s['labels'] for s in sentences]
        predictions = self.predict(sentences)
        r = utils.conlleval(labels, predictions)

        if compact:
            print(r.compact(header=header, comment=comment), file=file)
        elif output:
            print(r, file=file)

        if output_comp:
            with open(output_comp, 'w') as f:
                tokens = [s['tokens'] for s in sentences]
                for st, stl, spl in zip(tokens, labels, predictions):
                    f.write('\n')
                    for t, tl, pl in zip(st, stl, spl):
                        f.write('{}\t{}\t{}\n'.format(t, tl, pl))

        return r.f1


def load_data(fpath):
    if fpath.endswith('.conll') or fpath.endswith('.txt'):
        return utils.load_data_from_conll(fpath)
    elif fpath.endswith('.json'):
        with open(fpath) as f:
            return json.load(f)
    raise Exception('Unsupported data format')


def train(c: Config):
    os.makedirs(c.dir, exist_ok=True)

    fh = logging.FileHandler(os.path.join(c.dir, 'log.txt'))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter('%(asctime)s : %(name)s : %(levelname)s : %(message)s'))
    logging.getLogger().addHandler(fh)
    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return
        logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    sys.excepthook = handle_exception

    logger.info('-- Train -- \n{}'.format(c))

    try:
        from tensorflow.python.client import device_lib
        logger.info('{}'.format(device_lib.list_local_devices()))
    except Exception as e:
        logger.warning(f'Unable to get device info {e}')

    os.makedirs(c.dir, exist_ok=True)

    embedder = Embedder(c.embedder)
    embedder.fit(load_data(c.train))

    m = LstmTagger(config=c, embedder=embedder)
    m.build(initialize_weights=True)
    m.model.summary(print_fn=logger.info)
    m.save(c.dir, config_only=True)

    train_data = load_data(c.train)
    train_steps, train_gen = batch_iter(*embedder.transform(train_data, with_outputs=True), batch_size=c.batch_size)

    valid_data = load_data(c.dev)
    valid_steps, valid_gen = batch_iter(*embedder.transform(valid_data, with_outputs=True), batch_size=c.batch_size)

    cb = []
    cb.append(F1score(valid_data, embedder, model_dir=c.dir))
    cb.append(CSVLogger(os.path.join(c.dir, 'results.csv'), append=True))
    cb.append(TensorBoard(c.dir))
    cb.append(ModelCheckpoint(os.path.join(c.dir, 'weights_{epoch:02d}_{f1:2.2f}.h5'), monitor='f1', save_weights_only=True))
    cb.append(EarlyStopping(monitor='f1', patience=c.patience, mode='max'))

    m.model.fit_generator(train_gen, train_steps, epochs=c.epochs, validation_data=valid_gen, validation_steps=valid_steps, callbacks=cb)
    m.save(c.dir, model_only=True)
    scripts.pack(c.dir, c.dir, gz=True)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

    if '-h' in sys.argv:
        Config().help()
        sys.exit(0)

    config = Config().from_args(sys.argv[1:])

    train(config)

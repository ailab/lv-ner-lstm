import numpy as np
import logging
import gzip

logger = logging.getLogger('embeddings')


class Embeddings(object):
    def __init__(self, token_id, token_emb, dim, init_tokens=None):
        self.token_id = token_id or {}
        self.token_emb = token_emb or {}
        self.dim = dim

        if init_tokens:
            for t in init_tokens:
                self.token_id[t] = len(self.token_id)
                self.token_emb[t] = np.random.uniform(-0.25, 0.25, dim)

    @property
    def id_token(self):
        return {v: k for k, v in self.token_id.items()}

    @property
    def token_weights(self):
        w = np.zeros((len(self.token_id), self.dim))
        for t, idx in self.token_id.items():
            w[idx] = self.token_emb[t]
        return w

    def __str__(self):
        sample_emb = []
        for i in range(min(len(self.token_id), 5)):
            token = self.id_token[i]
            sample_emb.append((i, token, self.token_emb[token]))
        return 'Embeddings[dim={} len={}]:\n{}'.format(self.dim, len(self.token_id), '\n'.join(['\t#{} "{}" norm={} {} ...'.format(idx, t, np.linalg.norm(v), v[:10].tolist()) for idx, t, v in sample_emb]))

    def save(self, fpath):
        with open(fpath, 'w') as fout:
            for token, idx in self.token_id.items():
                emb = self.token_emb[token]
                fout.write('{} {}\n'.format(token, ' '.join((str(x) for x in emb))))

    @classmethod
    def load(cls, fpath, dim=100, keep_tokens=None, init_tokens=None):
        logger.info('Load embeddings from {} ..'.format(fpath))
        m = Embeddings(None, None, dim=dim, init_tokens=init_tokens)
        token_id = m.token_id
        token_emb = m.token_emb

        first = True
        with gzip.open(fpath, 'rt') if fpath.endswith('.gz') else open(fpath, encoding='utf8') as f:
            for line in f:
                splits = line.strip().split(' ')
                token = splits[0]
                if first:
                    if len(splits) == 2:
                        vector_dim = int(splits[1])
                        if vector_dim != dim:
                            raise Exception('Not expected vector dimension: {} instead of {}'.format(vector_dim, dim))
                        continue
                    else:
                        vector_dim = len(splits) - 1
                        if vector_dim != dim:
                            raise Exception('Not expected vector dimension: {} instead of {}'.format(vector_dim, dim))
                        first = False

                if not keep_tokens or token in keep_tokens:
                    if token not in token_id:
                        token_id[token] = len(token_id)
                        token_emb[token] = np.array([float(num) for num in splits[1:]])
        logger.info('Finished loading. {}'.format(m))
        return m


def get_embedding_weights(emb, voc, dim=None, norm=False):
    dim = dim or len(next(iter(emb.values())))
    w = np.zeros((len(voc), dim))

    for t, idx in voc.items():
        # w[idx] = emb.get(t, np.random.uniform(-0.25, 0.25, dim))
        if t in emb:
            w[idx] = emb[t]
    if norm:
        # w /= np.apply_along_axis(np.linalg.norm, -1, w)
        w /= (np.linalg.norm(w, axis=-1) + 0.00001).reshape((w.shape[0], 1))
    return w

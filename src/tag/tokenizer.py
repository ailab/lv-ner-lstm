#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from pprint import pprint

_re_word_start = r"[^\(\"\`{\[:;&\#\*@\)}\]\-,\.]"
"""Excludes some characters from starting word tokens"""

_re_non_word_chars = r"(?:[?!)\";}\]\*:@\'\({\[.,])"
"""Characters that cannot appear within words"""

# Order matters: longer expressions with prefix that matches other expression should be in beginning
# https://bytes.com/topic/python/answers/534624-how-get-longest-possible-match-pythons-module
_re_punct_exceptions = r'''
\d{2}\.\d{2}\.\d{4}     # dates
|\d+\-\d+               # streets, number intervals
#|\d+(?:\.\d{3})*(?:,\d+|,\-{1,2})?
|\d+(?:(?:\.\d{3})*(?:,\d+|,\-{1,2})|\.(?!\d))      # money sequences, ordinals (1., 2., ...)
|(?:
    \d+|
    I|II|III|IV|V|
    Dr|geb|Inh|[Pp]rof|med|Ing|jun|Mag|gen|Dipl|
    Str|St|Hs|Nr|[\w\-]+[Ss]tr|
    Abs|Ziff|No|bzgl|lfd|Reg|
    C[Oo]|e\.[Vv]|KG|L[Tt][Dd]|[[Kk][Ff][Mm]|[Kk]fr|
    (?:[A-Za-z]\.)*[A-Za-z]
)\.
'''

_word_tokenize_fmt = r'''(
    (?:%(Exceptions)s)
    |
    (?=%(WordStart)s)\S+?                   # Accept word characters until end is found
    (?=                                     # Sequences marking a word's end
        \s|                                 # White-space
        $|                                  # End-of-string
        %(NonWord)s                         # Punctuation
    )
    |
    \S
)'''

_re_word_tokenizer = re.compile(
    _word_tokenize_fmt %
    {
        'NonWord':   _re_non_word_chars,
        'WordStart': _re_word_start,
        'Exceptions': _re_punct_exceptions
    },
    re.UNICODE | re.VERBOSE
)

_re_newline_tokenize = re.compile(r'([^\n]*\n)', re.UNICODE)

def newline_tokenize(s):
    res = [(s[m.start():m.end()], m.start(), m.end()) for m in _re_newline_tokenize.finditer(s)]
    return res

def word_tokenize(s):
    res = [(s[m.start():m.end()], m.start(), m.end()) for m in _re_word_tokenizer.finditer(s)]
    return res


def tokenize(s):
    lines = newline_tokenize(s)
    res = []
    sent = []
    for l in lines:
        text = l[0]
        start = l[1]
        end = l[2]
        words = word_tokenize(text)
        words = [(w_text, w_start+start,w_end+start) for w_text,w_start,w_end in words]
        if text.strip():
            if sent:
                sent.append(('<s>', start - 1, start))
            sent += words
        else:
            res.append(sent)
            sent = []
    if sent:
        res.append(sent)
    return res

# pprint(word_tokenize(u"""
#     Susanne Völker <s>
# Geschäftsführung | Programmleitung <s>
# T. 0561.598619-11 <s>
# E. info(at)grimmwelt.de <s>
# E. info@grimmwelt.de <s>
# """))
# pprint(newline_tokenize(u"""
#     Susanne Völker <s>
# Geschäftsführung | Programmleitung <s>
# T. 0561.598619-11 <s>
# E. info(at)grimmwelt.de <s>
# E. info@grimmwelt.de <s>
# """))

# pprint(tokenize(u"""
#     Susanne Völker
# Geschäftsführung | Programmleitung
# T. 0561.598619-11
# E. info(at)grimmwelt.de
#
# E. info@grimmwelt.de
# """))
#
# print('\n\ntest\n\n'.split('\n'))


_re_ws_tokenize = re.compile(r'(\w+|\S|\s+)', re.UNICODE)

def _ws_tokenize(s):
    res = [(s[m.start():m.end()], m.start(), m.end()) for m in _re_ws_tokenize.finditer(s)]
    return res

def ws_tokenize0(s, with_ws_token=False):
    tokens = _ws_tokenize(s)
    sentences = []
    sentence = []
    new_line = None
    for t in tokens:
        t_text = t[0]
        if t_text.count('\n') > 1:
            if sentence:
                sentences.append(sentence)
                sentence = []
        # elif '\n' in t_text:
            # sentence.append(('<nl>', t[1], t[2]))
            # new_line = ('<nl>', t[1], t[2])
        elif t_text.strip() == '':
            # if new_line:
            #     sentence.append(new_line)
            #     new_line = None
            # sentence.append(('<ws>', t[1], t[2]))
            pass
        else:
            if new_line:
                sentence.append(new_line)
                new_line = None
            sentence.append(t)
    if sentence:

        sentences.append(sentence)
    return sentences


def ws_tokenize(s, with_ws_token=False):
    tokens = _ws_tokenize(s)
    sentences = []
    sentence = []
    new_line = None
    for i, t in enumerate(tokens):
        t_text = t[0]
        if t_text.count('\n') > 0:
            if sentence:
                sentences.append(sentence)
                sentence = []
        # elif '\n' in t_text:
            # sentence.append(('<nl>', t[1], t[2]))
            # new_line = ('<nl>', t[1], t[2])
        elif t_text.strip() == '':
            # if new_line:
            #     sentence.append(new_line)
            #     new_line = None
            # sentence.append(('<ws>', t[1], t[2]))
            pass
        else:
            if new_line:
                sentence.append(new_line)
                new_line = None
            sentence.append(t)
            if t_text in list('.!?') and sentence and (i + 1 >= len(tokens) or not tokens[i+1][0].islower()):
                sentences.append(sentence)
                sentence = []
    if sentence:

        sentences.append(sentence)
    return sentences


if __name__ == '__main__':
    text = """Königstraße  52
    30175 Hannover
    Telefon 0511-388198 0
    Telefax 0511-388198 15
    
    
    Geschäftsführer: M. Blecker
    Rg. Hannover HRA 25545
    Umst.ID: DE 186186375
    """

    print(text.split('\n'))


    pprint(ws_tokenize(text))


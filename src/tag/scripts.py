import fire
import os
import tarfile
import logging

logger = logging.getLogger('tag.scripts')


def pack(model_path, out_path, weights_path=None, gz=False):
    if not os.path.isdir(model_path):
        if not weights_path: weights_path = model_path
        model_path = os.path.dirname(model_path)

    if os.path.isdir(out_path):
        out_path = os.path.join(out_path, os.path.basename(model_path) + ('.tar.gz' if gz else '.tar'))

    model_name = os.path.basename(os.path.splitext(out_path)[0])

    if not weights_path:
        if os.path.exists(os.path.join(model_path, 'weights.h5')):
            weights_path = 'weights.h5'
        else:
            best_score = 0
            best_path = None
            for f in os.listdir(model_path):
                if not 'weights' in f or not f.endswith('.h5'): continue
                name = f[:-3]
                score = float(name.rsplit('_')[-1])
                if score > best_score:
                    best_score = score
                    best_path = f
            if best_path:
                logger.info('Use best model found ({}): {}'.format(best_score, best_path))
                weights_path = best_path

    with tarfile.open(out_path, 'w:gz' if gz else 'w') as tar:
        for f in [
            'config.json',
            'embedder.json',
            'log.txt',
            weights_path,
        ]:
            p = os.path.join(model_path, f)
            # tar.add(p, arcname=os.path.join(model_name, f))
            tar.add(p, arcname=f)
    logger.info('finished')


def unpack(path, out_path=None):
    with tarfile.open(path, 'r:gz' if path.endswith('.gz') else 'r') as tar:
        tar.extractall(out_path)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    fire.Fire()

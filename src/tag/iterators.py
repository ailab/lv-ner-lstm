import logging
import random
import numpy as np
logger = logging.getLogger('tag.iter')


def batch_iter(xs, ys, batch_size=32):
    batch_count = 0

    len_ids_map = dict()
    for sid, y in enumerate(ys):
        slen = len(y)
        if slen in len_ids_map:
            len_ids_map[slen].append(sid)
        else:
            len_ids_map[slen] = [sid]

    logger.info('Sentence lengths (slen, count): {}'.format(sorted([(k, len(v)) for k, v in len_ids_map.items()], key=lambda x: x[0])))

    len_id_list = list(len_ids_map.items())

    for slen, ids in len_id_list:
        batch_count += int((len(ids) - 1) / batch_size) + 1

    def batch_generator():
        while True:
            batches = []
            for slen, ids in len_id_list:
                i = 0
                random.shuffle(ids)
                while i < len(ids):
                    batch_ids = ids[i:i+batch_size]
                    batch_input = [np.asarray(l) for l in list(zip(*[xs[i] for i in batch_ids]))]
                    batch_output = np.asarray([ys[i] for i in batch_ids])
                    batches.append((batch_input, batch_output))
                    i += batch_size
            random.shuffle(batches)
            for batch_input, batch_output in batches:
                yield batch_input, batch_output

    return batch_count, batch_generator()

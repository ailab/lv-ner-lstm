import os
from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS
import json
import logging
import re

from tag.tagger import LstmTagger
from tag.tokenizer import ws_tokenize

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger('api')

MODEL = os.getenv('MODEL', '../../models/lv_ner_sembank0.4-c0.1-m3-run0')
PORT = os.getenv('PORT', '9501')
HOST = os.getenv('HOST', '0.0.0.0')

app = Flask(__name__, static_folder=None)
CORS(app)

app.tagger = LstmTagger.load(MODEL)


def tokenize(text):
    sentences = []
    tokens = text.split()
    sentences.append(dict(tokens=tokens))
    return sentences


def sentences_from_spans(sent_token_spans):
    sentences = []
    for s in sent_token_spans:
        tokens = []
        starts = []
        ends = []
        for t, start, end in s:
            tokens.append(t)
            starts.append(start)
            ends.append(end)
        sentences.append(dict(tokens=tokens, start=starts, end=ends))
    return sentences


def get_sentences(s):
    # RE_WORD_PUNKT_TOKENIZER = re.compile(r'\w+|[^\w\s]+')
    RE_WORD_PUNKT_TOKENIZER = re.compile(r'(\w+|\S|\s+)', re.UNICODE)
    token_spans = [(s[m.start():m.end()], m.start(), m.end()) for m in RE_WORD_PUNKT_TOKENIZER.finditer(s)]
    res = [dict(
        text=s,
        tokens=[t[0] for t in token_spans],
        start=[t[1] for t in token_spans],
        end=[t[2] for t in token_spans],
        tokens_raw=token_spans,
    )]
    print(res)
    return res


@app.route('/ner_plain', methods=['GET', 'POST'])
def ner_plain():
    req = request.get_json(force=True, silent=True) or request.values
    text = req['text'] or ''
    logger.info('process: {}'.format(repr(text[:20])))
    entities = []
    try:
        if text:
            sentences = get_sentences(text)
            entities = app.tagger.predict_spans(sentences) if text else []
    except Exception as e:
        logger.exception('Exception on {}... {}'.format(repr(text), e))
    return jsonify(entities)


@app.route('/ner', methods=['POST'])
def ner():
    req = request.get_json(force=True, silent=True)
    doc = req['data']
    try:
        sentences = doc['sentences']
        sent_tokens = [dict(tokens=[t['form'] for t in s['tokens']]) for s in sentences]
        entities = app.tagger.predict_spans(sent_tokens)
        for sent in sentences:
            sent['ner'] = []
        for entity in entities:
            sentences[entity['sent_idx']]['ner'].append(dict(
                text=entity['text'],
                label=entity['label'],
                start=entity['start_idx'],
                end=entity['end_idx'],
                sentence_idx=entity['sent_idx'],
            ))
        return jsonify(dict(data=doc))
    except Exception as e:
        logger.exception('Exception on {}... {}'.format(doc, e))
        return jsonify(dict(error=str(e)))


@app.route('/')
def demo_index():
    return send_from_directory(os.path.abspath(os.path.join(os.path.dirname(__file__))), 'demo.html')


if __name__ == '__main__':
    print('RUN API %r %r' % (HOST, PORT))
    app.run(host=HOST, port=int(PORT), debug=False, threaded=False)

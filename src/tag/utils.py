import os
import subprocess
import re
import logging
import copy

logger = logging.getLogger('utils')


def load_data_from_conll(fpath, cols=None):
    if not cols:
        cols = dict(tokens=0, labels=-1)

    sentences = []
    sentence = {name: [] for name in cols}
    empty_sentence = True

    with open(fpath) as f:
        for line in f:
            line = line.strip()
            if not line or line.startswith('#') or '-DOCSTART-' in line:
                if not empty_sentence:
                    sentences.append(sentence)
                    sentence = {name: [] for name in cols}
                    empty_sentence = True
                continue

            splits = line.split()
            for col_name, col_idx in cols.items():
                sentence[col_name].append(splits[col_idx])
                empty_sentence = False

    if not empty_sentence:
        sentences.append(sentence)

    return sentences

def data_stats(data):
    print('count_sentences={}'.format(len(data)))
    len_counts = {}
    for s in data:
        slen = len(s['tokens'])
        if not slen in len_counts:
            len_counts[slen] = 1
        else:
            len_counts[slen] += 1
    len_counts = sorted(list(len_counts.items()), key=lambda x: x[0])
    print('Sentence lengths:')
    for slen, c in len_counts:
        print(slen, c)


def get_label_map(dataset):
    label2idx = {}
    for seq in dataset:
        for label in seq['labels']:
            if not label in label2idx:
                label2idx[label] = len(label2idx)
    return label2idx


class TagF1Result():
    def __init__(self):
        self.tag = None
        self.p = None
        self.r = None
        self.f1 = None
        self.count = None

    def __str__(self):
        return '{s.tag}\t{s.p}\t{s.r}\t{s.f1}\t{s.count}'.format(s=self)


class F1Result():
    def __init__(self):
        self.raw = None

        self.acc = -1

        self.p = -1
        self.r = -1
        self.f1 = -1

        self.count_correct = -1
        self.count_tokens = -1
        self.count_phrases = -1

        self.tags = []

    def dic(self):
        r = copy.deepcopy(self.__dict__)
        r['tags'] = [e.__dict__ for e in r['tags']]
        return r

    def __str__(self):
        return '{raw}\n' \
               '{tags}\n' \
               'TOTAL\t{s.p}\t{s.r}\t{s.f1}\n' \
               'tokens={s.count_tokens}, phrases={s.count_phrases}, correct={s.count_correct}, acc={s.acc}'\
            .format(raw=self.raw, s=self, tags='\n'.join([str(t) for t in self.tags]))

    def compact(self, comment='', header=True):
        s = ('F1     P      R      ' + ' '.join('{:<20}'.format(t.tag) for t in self.tags) + ' \n') if header else ''
        s += '{s.f1:<6.2f} {s.p:<6.2f} {s.r:<6.2f} {tags} {comment}'.format(
            s=self,
            tags=' '.join(['{:<20}'.format('{s.f1:>.2f} {s.p:>.2f} {s.r:>.2f}'.format(s=t)) for t in self.tags]),
            comment=comment
        )
        return s

    @classmethod
    def from_conlleval(cls, s):
        r = cls()
        r.raw = s
        lines = s.split('\n')
        try:
            m = re.search(r"processed ([\d]+) tokens with ([\d]+) phrases; found: ([\d]+) phrases; correct: ([\d]+)\.",
                          lines[0])
            r.count_tokens = int(m.group(1))
            r.count_phrases = int(m.group(2))
            r.count_correct = int(m.group(4))

            m = re.search(r"accuracy:\s*([\d.]+)%; precision:\s*([\d.]+)%; recall:\s*([\d.]+)%; FB1:\s*([\d.]+)", lines[1])
            r.acc = float(m.group(1))
            r.p = float(m.group(2))
            r.r = float(m.group(3))
            r.f1 = float(m.group(4))

            for l in lines[2:]:
                if not l.strip(): continue
                m = re.search(r"\s*([^\s:]+):\s+precision:\s+([\d.]+)%;\s+recall:\s+([\d.]+)%;\s+FB1:\s+([\d.]+)\s+([\d]+)",
                              l)
                t = TagF1Result()
                t.tag = m.group(1)
                t.p = float(m.group(2))
                t.r = float(m.group(3))
                t.f1 = float(m.group(4))
                t.count = int(m.group(5))
                r.tags.append(t)

        except Exception:
            logger.exception('Unable to parse conlleval result')

        return r


def conlleval(ref_labels, hyp_labels, labelmap=None):
    # print(labelmap)
    # print('hyp_labels', hyp_labels)
    if labelmap:
        ref_labels = [[labelmap[l] for l in sent_labels] for sent_labels in ref_labels]
        hyp_labels = [[labelmap[l] for l in sent_labels] for sent_labels in hyp_labels]
    s = []
    # print('ref_labels')
    for ref_sent_labels, hyp_sent_labels in zip(ref_labels, hyp_labels):
        assert len(ref_sent_labels) == len(hyp_sent_labels)
        for ref, hyp in zip(ref_sent_labels, hyp_sent_labels):
            # print(ref, hyp)
            s.append(' '.join(['', ref, hyp]))  # add dummy value, so that conlleval doesn't generate a lot of warnings
        s.append('')
    s = '\n'.join(s)
    # print(s)

    eval_script = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'conlleval')
    p = subprocess.Popen(['perl', eval_script], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    p.stdin.write(s.encode())
    p.stdin.close()

    while p.returncode is None:
        p.poll()

    eval_result_str = p.stdout.read().decode()

    # print(eval_result_str)
    r = F1Result.from_conlleval(eval_result_str)
    return r


def get_token_shape(token: str):
    r = []
    for c in token:
        if c.isdigit(): r.append('0')
        elif c.isalpha(): r.append('a' if c.islower() else 'A')
        elif c == '-': r.append('-')
        elif c == '.': r.append('.')
        else: r.append('x')
    if len(r) > 3:
        _r = [r[0]]
        for i, c in enumerate(r[1:]):
            if c != r[i]: _r.append(c)
        r = _r

    return ''.join(r)


def get_token_case(word: str):
    casing = 'other'

    numDigits = 0
    for char in word:
        if char.isdigit():
            numDigits += 1

    digitFraction = numDigits / float(len(word))

    if word.isdigit():  # Is a digit
        casing = 'numeric'
    elif digitFraction > 0.5:
        casing = 'mainly_numeric'
    elif word.islower():  # All lower case
        casing = 'allLower'
    elif word.isupper():  # All upper case
        casing = 'allUpper'
    elif word[0].isupper():  # is a title, initial char upper, then all lower
        casing = 'initialUpper'
    elif numDigits > 0:
        casing = 'contains_digit'

    return casing

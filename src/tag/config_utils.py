import json


class Option():
    def __init__(self, *args, **kwargs):
        self.default = kwargs.get('default', None)
        self.choices = kwargs.get('choices', None)
        self.items = kwargs.get('items', None)
        self.required = kwargs.get('required', True)

    def validate(self, val):
        assert (not self.required or val is not None)
        assert(not self.choices or val in self.choices)
        assert(not self.items or all([e in self.items for e in val]))
        return val

    def __str__(self):
        return '{}({})'.format(type(self).__name__, self.__dict__)


class Int(Option):
    def __init__(self, *args, **kwargs):
        super(Int, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate(int(val))


class Float(Option):
    def __init__(self, *args, **kwargs):
        super(Float, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate(float(val))


class IntList(Option):
    def __init__(self, *args, **kwargs):
        super(IntList, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate([int(e) for e in val.split(',')])


class FloatList(Option):
    def __init__(self, *args, **kwargs):
        super(FloatList, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate([float(e) for e in val.split(',')])


class Str(Option):
    def __init__(self, *args, **kwargs):
        super(Str, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate(str(val))


class Json(Option):
    def __init__(self, *args, **kwargs):
        super(Json, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate(json.loads(val))


class StrList(Option):
    def __init__(self, *args, **kwargs):
        super(StrList, self).__init__(*args, **kwargs)

    def __call__(self, val):
        return self.validate(str(val).split(','))


class Bool(Option):
    def __init__(self, *args, **kwargs):
        super(Bool, self).__init__(*args, **kwargs)

    def __call__(self, val):
        if str(val).lower() in {'true', 'yes', '1'}: return self.validate(True)
        if str(val).lower() in {'false', 'no', '0'}: return self.validate(False)
        return self.validate(None)


class BaseConfig(object):
    def __init__(self):
        pass

    def dic(self):
        return {k: getattr(self, k) for k in dir(self) if not k.startswith('_') and not callable(getattr(self, k))}

    def init(self, dic):
        for k, v in dic.items():
            setattr(self, k, v)

    def __str__(self):
        return '{}({})'.format(type(self).__name__, json.dumps(self.dic()))

    def save(self, fpath):
        with open(fpath, 'w') as fout:
            fout.write(json.dumps(self.dic(), indent=2))

    @classmethod
    def load(cls, fpath):
        with open(fpath) as f:
            data = json.load(f)
        r = cls()
        r.init(data)
        return r

    def from_args(self, args):
        # print(self.__annotations__)
        arg_values = []

        cur_arg = None
        for i, arg in enumerate(args):

            if arg.startswith('--'):
                if cur_arg: pass
                cur_arg = arg[2:]
            else:
                if cur_arg:
                    arg_values.append((cur_arg, arg))

        for arg, val in arg_values:
            field = self.__annotations__[arg]
            setattr(self, arg, field(val))
        return self

    def help(self):
        for arg, option in self.__annotations__.items():
            print(arg, option)


if __name__ == '__main__':

    class C(BaseConfig):
        lstm_units: IntList() = (100, 75)
        token_emb_dim: Int(required=True) = 100
        dropout: Float(required=True) = 0.25
        recurrent_dropout: Float(required=True) = 0.25
        char_emb_dim: Int(required=True) = 25
        char_lstm_units: Int(required=True) = 25
        shape_emb_dim: Int(required=True) = 10
        case_emb_dim: Int(required=True) = 7
        features: StrList(required=True, items='token char'.split()) = ['token']
        loss: Str(required=True, choices='crf categorical_crossentropy'.split()) = 'crf'

    c = C()
    # c.help()
    c.from_args('--token_emb_dim 1 --features token,char --loss crf'.split())
    print(c)

class Vocab(object):
    def __init__(self):
        self.token2id = {}
        self.token2count = {}

    def add(self, tokens, lower=False):
        if tokens is not None:
            for t in tokens:
                if lower:
                    t = t.lower()
                if t not in self.token2id:
                    self.token2id[t] = len(self.token2id)
                if t not in self.token2count:
                    self.token2count[t] = 1
                else:
                    self.token2count[t] += 1
        return self

    def filter(self, min_count=2):
        new_token2count = {}
        new_token2id = {}

        v = list(self.token2count.items())
        v.sort(key=lambda x: -x[1])
        for token, count in v:
            if count < min_count:
                break
            new_token2count[token] = count
            new_token2id[token] = len(new_token2id)

        self.token2count = new_token2count
        self.token2id = new_token2id
        return self

    def most_common(self, n=None):
        items = list(self.token2count.items())
        items.sort(key=lambda x: x[1], reverse=True)
        if n is not None:
            items = items[:n]
        return items

    def first(self, n=None):
        items = list(self.token2id.items())
        items.sort(key=lambda x: x[1])
        if n is not None:
            items = items[:n]
        return items

    def __len__(self):
        return len(self.token2id)

    def __contains__(self, item):
        return item in self.token2id

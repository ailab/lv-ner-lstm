import os
import logging
import re
from flask import Flask, jsonify, request, send_from_directory, redirect, url_for
from flask_cors import CORS
from tag.tagger import LstmTagger


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger('api')

NER_MODEL = os.getenv('NER_MODEL')
PORT = os.getenv('PORT', '9501')
HOST = os.getenv('HOST', '0.0.0.0')

app = Flask(__name__)
CORS(app)
app.tagger = LstmTagger.load(NER_MODEL)


RE_WORD_PUNKT_TOKENIZER = re.compile(r'\w+|[^\w\s]+')


def ws_tokenize(s):
    res = [(s[m.start():m.end()], m.start(), m.end()) for m in RE_WORD_PUNKT_TOKENIZER.finditer(s)]
    return res


RE_WORD_PUNKT_NL_TOKENIZER = re.compile(r'\n|\w+|[^\w\s]+')


def ws_nl_tokenize(s):
    res = [(s[m.start():m.end()] if s[m.start():m.end()] != '\n' else '<nl>', m.start(), m.end()) for m in RE_WORD_PUNKT_NL_TOKENIZER.finditer(s)]
    return res


def get_sentences(s):
    # token_spans = ws_tokenize(s)
    token_spans = ws_nl_tokenize(s)
    token_spans.append(('<nl>', len(s), len(s)))
    res = [dict(
        text=s,
        tokens=[t[0] for t in token_spans],
        start=[t[1] for t in token_spans],
        end=[t[2] for t in token_spans],
        tokens_raw=token_spans,
    )]
    return res


@app.route('/')
def index():
    # return 'LMT Demo'
    return redirect(url_for('ner_demo'))


@app.route('/api/ner', methods=['GET', 'POST'])
def api_ner():
    req = request.get_json(force=True, silent=True) or request.values
    text = req['text'] or ''
    logger.info('process: {}'.format(repr(text[:20])))
    entities = []
    try:
        if text:
            sentences = get_sentences(text)
            entities = app.tagger.predict_spans(sentences) if text else []
    except Exception as e:
        logger.exception('Exception on {}... {}'.format(repr(text), e))
    return jsonify(entities)


@app.route('/ner')
def ner_demo():
    return send_from_directory(os.path.abspath(os.path.join(os.path.dirname(__file__), 'tag')), 'demo.html')


if __name__ == '__main__':
    print('RUN API %r %r' % (HOST, PORT))
    app.run(host=HOST, port=int(PORT), debug=False, threaded=False)

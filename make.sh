#!/usr/bin/env bash

IMG=registry.gitlab.com/ailab/lv-ner-lstm

if [ $# -eq 0 ]; then
    TAG=latest
    echo "Build local ${IMG}:${TAG}"
    docker build -t $IMG:$TAG .
fi

if [ "$1" == "push" ]; then
    TAG=${2:-latest}
    echo "Push ${IMG}:${TAG}"
    docker tag $IMG:latest $IMG:$TAG \
    && docker push $IMG:$TAG
fi

if [ "$1" == "git" ]; then
    echo "Build/push"
    GIT_REV=$(git rev-parse --short=8 HEAD)
    echo "Tag (default=$GIT_REV):"
    read TAG
    TAG=${TAG:-$GIT_REV}
    echo  "TAG='$TAG'"
    docker build -t $IMG:$TAG . && docker push $IMG:$TAG \
        && docker tag $IMG:$TAG $IMG:latest && docker push $IMG:latest
fi